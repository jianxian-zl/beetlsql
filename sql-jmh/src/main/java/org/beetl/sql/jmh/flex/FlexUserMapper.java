package org.beetl.sql.jmh.flex;

import com.mybatisflex.core.BaseMapper;

public interface FlexUserMapper extends BaseMapper<FlexSysUser> {
}
